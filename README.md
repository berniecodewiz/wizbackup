Wizbackup is a minimalistic backup system based on rsync and hardlinked
snapshots.

Its main feature is that each backup snapshot is a plain filesystem tree, identical to the
original. Theere's no need for a tool to restore and manage backups.

There are a few downsides to this simple structure:
 - Backups are uncompressed (unless the underlying filesystem supports transparent complression)
 - Large files which change every day, such as logs and databases, are duplicated in each snapshot,
   wasting space. Reflinks (aka COW-links) would solve this.


Setup
=====

List the hosts to be backed up:

```
mkdir -p /backup/HOSTS
cat >/backup/HOSTS/example <<__EOF__
host1.example.com
host2.example.com
__EOF__
```

Optionally, specify paths to be excluded from backups:

```
mkdir -p /backup/EXCLUDE
cat >/backup/EXCLUDE/ALWAYS <<__EOF__
/dev/
/mnt/
/proc/
/sys/
/selinux/
__EOF__
```

You can also specify host-specific excludes:

```
cat >/backup/EXCLUDE/host1.example.com <<__EOF__
/var/cache
__EOF__
```


Authorizing wizbackup
=====================

Wizbackup requires root access in order to read the entire filesystem of a host.
Create an ssh keypair and install the public key on each host to be backed up:

```
mkdir -p /etc/wizbackup
ssh-keygen -t ed25519 -N '' -C wizbackup@example.com -f /etc/wizbackup/ssh_id
ssh-copy-id -f /etc/wizbackup/ssh_id.pub root@host1.example.com
ssh-copy-id -f /etc/wizbackup/ssh_id.pub root@host2.example.com
```

You might also need to change the value of PermitRootLogin in `/etc/ssh/sshd_config`.


Running as a Systemd timer
==========================

```
ln -s $PWD/systemd/wizbackup.timer /etc/systemd/system/wizbackup.timer
ln -s $PWD/systemd/wizbackup.service /etc/systemd/system/wizbackup.service
systemctl enable wizbackup.timer
```

The service will log to journals.
For testing, you can manually trigger wizbackup and examine its status:

```
systemctl start wizbackup.service
systemctl status wizbackup.service
```


Running as a Cron job
=====================

```
cat >/etc/cron.daily/wizbackup <<__EOF__
#!/bin/bash
wizbackup-driver /backup/HOSTS/example /backup
__EOF__
```

That's it!
